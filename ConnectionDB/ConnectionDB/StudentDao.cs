﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectionDB
{
    class StudentDao : IDao<StudentDto>
    {
        public void Delete(StudentDto student)
        {
            using (SqlConnection connection = DatabaseSingleton.Database.Connection)
            {
                connection.Open();

                List<string> clauses = new List<string>();

                if (student.RollList != null)
                    clauses.Add("Roll_list = @id");
                if (student.Name != null)
                    clauses.Add("Name = @name");
                if (student.Surname != null)
                    clauses.Add("Surname = @surname");
                if (student.DateOfBirth != null) ;
                clauses.Add("Date_of_birth = @dateOfBirth");

                string deleteString = $"DELETE FROM dbo.Student WHERE {BuildClause(clauses, "AND")};";

                SqlCommand command = new SqlCommand(deleteString, connection);
                command.Parameters.AddWithValue("@rollList", student.RollList);
                command.Parameters.AddWithValue("@name", student.Name);
                command.Parameters.AddWithValue("@surname", student.Surname);
                command.Parameters.AddWithValue("@dateOfBirth", student.DateOfBirth);

                command.ExecuteReader();
            }
        }

        
        public StudentDto Get(int id)
        {
            StudentDto student = null;
            using (SqlConnection connection = DatabaseSingleton.Database.Connection)
            {
                connection.Open();

                string queryString = @"SELECT * FROM dbo.Student WHERE Roll_list = @id;";

                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@id", id);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                        student = new StudentDto(reader.GetInt32(0),
                                                 reader.GetString(1),
                                                 reader.GetString(2),
                                                 reader.GetDateTime(3));

                }
            }
            return student;
        }

        public IList<StudentDto> GetAll()
        {
            List<StudentDto> students = new List<StudentDto>();
            using (SqlConnection connection = DatabaseSingleton.Database.Connection)
            {
                connection.Open();

                string queryString = "SELECT * FROM dbo.Student;";

                SqlCommand command = new SqlCommand(queryString, connection);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        students.Add(new StudentDto(reader.GetInt32(0),
                                                    reader.GetString(1), 
                                                    reader.GetString(2), 
                                                    reader.GetDateTime(3)));
                    }
                }
            }
            return students;
        }

        public void Insert(StudentDto student)
        {
            using (SqlConnection connection = DatabaseSingleton.Database.Connection)
            {
                connection.Open();
                
                string insertString = "INSERT INTO dbo.Student(Name, Surname,Date_of_birth) " +
                                      "VALUES (@name, @surname, @dateOfBirth)";

                SqlCommand command = new SqlCommand(insertString, connection);
                command.Parameters.AddWithValue("@name", student.Name);
                command.Parameters.AddWithValue("@surname", student.Surname);
                command.Parameters.AddWithValue("@dateOfBirth", student.DateOfBirth);
                //command.Parameters.Add("@name", SqlDbType.VarChar, 20).Value = t.Name;

                command.ExecuteReader();
            }
        }

        public void Update(StudentDto student)
        {
            if (student.RollList != null)
                throw new ArgumentException();

            using (SqlConnection connection = DatabaseSingleton.Database.Connection)
            {
                connection.Open();

                List<string> clauses = new List<string>();

                if (student.Name != null)
                    clauses.Add("Name = @name");
                if (student.Surname != null)
                    clauses.Add("Surname = @surname");
                if (student.DateOfBirth != null) ;
                clauses.Add("Date_of_birth = @dateOfBirth");

                string updateString = $"UPDATE dbo.Student SET {BuildClause(clauses, ",")} WHERE Roll_list = @rollList;";

                SqlCommand command = new SqlCommand(updateString, connection);
                command.Parameters.AddWithValue("@rollList", student.RollList);
                command.Parameters.AddWithValue("@name", student.Name);
                command.Parameters.AddWithValue("@surname", student.Surname);
                command.Parameters.AddWithValue("@dateOfBirth", student.DateOfBirth);

                command.ExecuteReader();
            }
        }

        private string BuildClause(IList<string> clauses, string mainConnector)
        {
            if (clauses is null)
                throw new ArgumentNullException(nameof(clauses));

            string clause = "";

            for (int i = 0; i < clauses.Count; i++)
            {
                clause += clauses[i];
                if (i < clauses.Count - 1)
                    clause += $" {mainConnector} ";
            }

            return clause;
        }
    }


}
