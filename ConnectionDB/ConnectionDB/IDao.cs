﻿using System.Collections.Generic;

namespace ConnectionDB
{
    /// <summary>
    /// Main CRUD operations
    /// </summary>
    /// <typeparam name="T"></typeparam>
    interface IDao<T> where T : IDto
    {
        /// <summary>
        /// Search for an item in the collection
        /// </summary>
        /// <param name="id">Unique identifier</param>
        /// <returns>Object that has id as identifier</returns>
        T Get(int id);

        IList<T> GetAll();

        void Insert(T t);

        void Update(T t);

        void Delete(T t);
    }
}
