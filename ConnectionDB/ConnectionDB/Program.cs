﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectionDB
{
    class Program
    {
        static void Main(string[] args)
        {
            StudentDao studentDao = new StudentDao();
            StudentDto student;

            //studentDao.Delete(new StudentDto("22Nome2", "Cognome3", new DateTime(1980, 4,13)));

            Console.WriteLine("Test Get():");
            for (int i = 0; i < 10; i++)
            {
                student = studentDao.Get(i);
                if (student == null)
                    Console.WriteLine($"{i}) Non presente");
                else
                    Console.WriteLine($"{i}) {student}");
            }

            Console.WriteLine();
            Console.WriteLine("Test GetAll():");
            foreach (StudentDto s in studentDao.GetAll())
            {
                Console.WriteLine(s);
            }

            student = new StudentDto(4, "Nome", "Cognome", new DateTime(1985,4,13));
            //studentDao.Insert(student);

            studentDao.Update(student);

            Console.WriteLine();
            Console.WriteLine("Test GetAll():");
            foreach (StudentDto s in studentDao.GetAll())
            {
                Console.WriteLine(s);
            }

            Console.ReadLine();
        }
        
    }
}
