﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectionDB
{
    class StudentDto : IDto
    {
        private int _rollList;
        private string _name;
        private string _surname;
        private DateTime _dateOfBirth;

        public string RollList => $"N86{_rollList.ToString("D6")}";
        public string Name => _name;
        public string Surname => _surname;
        public DateTime DateOfBirth => _dateOfBirth;

        public StudentDto(int rollList, string name, string surname, DateTime dateOfBirth)
        { 
            if (String.IsNullOrEmpty(name) || String.IsNullOrEmpty(surname) || dateOfBirth == null)
                throw new ArgumentException();

            _rollList = rollList;
            _name = name;
            _surname = surname;
            _dateOfBirth = dateOfBirth;
        }

        public StudentDto(string name, string surname, DateTime dateOfBirth) : 
               this(-1, name, surname, dateOfBirth)
        { }

        public override string ToString()
        {
            return $"{_name} {_surname}";
        }
    }
}
