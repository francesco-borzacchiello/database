﻿using ConnectionDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentsWithGUITable
{
    class Controller
    {
        private Model _model;
        private Table _view;
        public Model Model
        {
            get =>_model;
            set
            {
                if (value != null)
                    _model = value;
            }
        }

        public Table View
        {
            get => _view;
            set
            {
                if (value != null)
                    _view = value;
            }
        }

        public Controller() {}

        public Controller(Model m, Table v)
        {
            _view = v;
            _model = m;
            //MessageBox.Show("Controller(Model m, Table v)");
        }

        public void Start()
        {
            //_view.ShowDialog();
            //_view.InitTable<StudentDto>(_model.GetListStudentRandom(5));

            Application.Run(_view);
            MessageBox.Show("Run()");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _view.InitTable<StudentDto>(_model.GetListStudentRandom(5));
        }

        public void Table_Load(object sender, EventArgs e)
        {
            MessageBox.Show("Table_Load()");
            //_view.InitTable<StudentDto>(_model.GetListStudentRandom(5));
        }
    }
}
