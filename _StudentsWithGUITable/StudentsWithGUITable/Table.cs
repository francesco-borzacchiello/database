﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentsWithGUITable
{
    public partial class Table : Form
    {
        public Table()
        {
            InitializeComponent();
            //dataGridView1.DataSource = getListStudentRandom(5);
        }

        public void InitTable<T>(List<T> content)
        {
            dataGridView1.DataSource = content;
        }

        
    }
}
