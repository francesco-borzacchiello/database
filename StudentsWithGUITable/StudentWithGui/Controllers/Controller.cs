﻿using StudentsWithGUITable.Views;
using StudentsWithGUITable.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace StudentsWithGUITable.Controllers
{
    class Controller
    {
        private StudentDao _model;
        private TableView _view;
        public StudentDao Model
        {
            get => _model;
            set
            {
                if (value != null)
                    _model = value;
            }
        }

        public TableView View
        {
            get => _view;
            set
            {
                if (value != null)
                    _view = value;
            }
        }

        public Controller(StudentDao m, TableView v)
        {
            _view = v;
            _model = m;
        }

        public void Start() => PrepareTableForStudent();

        private void PrepareTableForStudent()
        {
            _view.Text = "Elenco Studenti Informatica";
            AddEventHandler();

            Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(_view);
        }

        /// <summary>
        /// Adds event handlers to the view
        /// </summary>
        private void AddEventHandler()
        {
            _view.Load += new EventHandler(Table_Load);
            _view.GetDataGridView().SelectionChanged += new EventHandler(Table_SelectionChanged);
            _view.GetDataGridView().MouseClick += new MouseEventHandler(Table_MouseClick);
        }

        /// <summary>
        /// Load the elements in the table when the form is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Table_Load(object sender, EventArgs e)
        {
            _view.InitTable<StudentDto>(_model.GetAll());
        }

        private void Table_SelectionChanged(object sender, EventArgs e)
        {
            Object obj = _view.UpdateCurrentlySelectedRow();

            if(obj != null && obj.GetType().Equals(typeof(StudentDto)))
            {
                Debug.WriteLine($"obj != null && obj.GetType().Equals(typeof(StudentDto)) = {obj != null && obj.GetType().Equals(typeof(StudentDto))}");
                //Cast sicuro
                UpdateDatabaseContent((StudentDto)obj);
                /*StudentDto student = (StudentDto)obj;
                StudentDao studentDao = new StudentDao();
                StudentDto newStudent = studentDao.Get(Int32.Parse(student.RollList));
                if (newStudent == null)
                    studentDao.Insert(newStudent);
                if (!student.Equals(newStudent))
                    studentDao.Update(newStudent);*/
            }
        }

        private void Table_MouseClick(object sender, MouseEventArgs e)
        {
            ContextMenuStrip menu = _view.PopUpMenuForDeleting(e);
            if (menu != null)
                menu.ItemClicked += new ToolStripItemClickedEventHandler(MessageBoxToConfirmTheDeletion);
            Debug.WriteLine("tornato in Table_MouseClick()");
        }

        private void MessageBoxToConfirmTheDeletion(Object sender, ToolStripItemClickedEventArgs e)
        {
            ContextMenuStrip menu = (ContextMenuStrip)sender;
            menu.Hide();
            Object obj = _view.UpdateCurrentlySelectedRow();

            if (obj != null && obj.GetType().Equals(typeof(StudentDto)))
            {
                StudentDto student = (StudentDto)obj;
                DialogResult answer = MessageBox.Show($"Sei sicuro di voler eliminare lo studente {student} ?",
                            "Conferma eliminazione", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (answer == DialogResult.Yes)
                {
                    _view.DaleteCurrentlySelectedRow();
                    _model.Delete(student);
                }
            }
        }

        private void UpdateDatabaseContent(StudentDto student)
        {
            StudentDto studentInDb = _model.Get(student.Id);
            //Se lo studente non esiste, viene inserito...
            if (studentInDb == null)
                _model.Insert(student);
            else
            {
                //...altrimenti se esiste, viene modificato.
                if (!student.Equals(studentInDb))
                    _model.Update(student);
            }
            
        }
    }
}
