﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsWithGUITable.Models
{
    class StudentDto : IDto
    {
        private int _rollList;
        private string _name;
        private string _surname;
        private DateTime _dateOfBirth;

        public string RollList => $"N86{_rollList.ToString("D6")}";
        public int Id => _rollList;
        public string Name
        {
            set
            {
                if (value != null)
                    _name = value;
            }
            get => _name;
        }
        public string Surname
        {
            set
            {
                if (value != null)
                    _surname = value;
            }
            get => _surname;
        }
        public DateTime DateOfBirth
        {
            set
            {
                if (value != null)
                    _dateOfBirth = value;
            }
            get => _dateOfBirth;
        }
        
        public StudentDto() { }

        public StudentDto(int rollList, string name, string surname, DateTime dateOfBirth)
        { 
            if (String.IsNullOrEmpty(name) || String.IsNullOrEmpty(surname) || dateOfBirth == null)
                throw new ArgumentException();

            _rollList = rollList;
            _name = name;
            _surname = surname;
            _dateOfBirth = dateOfBirth;
        }

        public StudentDto(string name, string surname, DateTime dateOfBirth) : 
               this(-1, name, surname, dateOfBirth)
        { }

        public override string ToString()
        {
            return $"{_name} {_surname} [{RollList}]";
        }

        public override bool Equals(object obj)
        {
            if (!obj.GetType().Equals(typeof(StudentDto)))
                return false;

            StudentDto student = (StudentDto)obj;

            return _rollList.Equals(student.RollList) && _name.Equals(student.Name) &&
                   _surname.Equals(student.Surname)   && _dateOfBirth.Equals(student.DateOfBirth);
        }

        public override int GetHashCode()
        {
            int hashCode = -1442037199;
            hashCode = hashCode * -1521134295 + _rollList.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_name);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(_surname);
            hashCode = hashCode * -1521134295 + _dateOfBirth.GetHashCode();
            return hashCode;
        }
    }
}
