﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using StudentsWithGUITable;

namespace StudentsWithGUITable.Models
{
    /// <summary>
    /// Establishes a connection with the SQL server database
    /// </summary>
    class DatabaseSingleton
    {
        /// <summary>
        /// Retrieve the configuration string from the file App.config
        /// </summary>
        private static readonly string _connectionString = ConfigurationManager.AppSettings["connectionString"];
        private static DatabaseSingleton _database;
        private static SqlConnection _connection;

        private DatabaseSingleton() { }

        public SqlConnection Connection
        {
            get { return _connection; }
        }

        public static DatabaseSingleton Database
        {
            get
            {
                if (_database == null)
                    _database = new DatabaseSingleton();

                _connection = new SqlConnection(_connectionString);
                return _database;
            }
        }
    }
}
