﻿// using StudentsWithGUITable.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentsWithGUITable.Views
{
    public partial class TableView : System.Windows.Forms.Form
    {
        private int _currentlySelectedRow = -1;

        public TableView() => InitializeComponent();

        public DataGridView GetDataGridView() => dataGridView1;

        public void InitTable<T>(IList<T> content)
        {
            //Uso un Binding source poichè con una List<> non posso aggiungere nuove righe alla tabella
            BindingSource bs = new BindingSource
            {
                DataSource = content
            };
            dataGridView1.DataSource = bs;
        }

        private Object GetObjectInTheCurrentlySelectedRow()
        {
            if (_currentlySelectedRow > 0)
                return dataGridView1.Rows[_currentlySelectedRow].DataBoundItem;
            return null;
        }

        public Object UpdateCurrentlySelectedRow()
        {
            if (dataGridView1.SelectedCells.Count > 0)
            {
                //Stato iniziale
                if (_currentlySelectedRow < 0)
                {
                    _currentlySelectedRow = dataGridView1.SelectedCells[0].RowIndex;
                }
                else
                {
                    int newCurrentlySelectedRow = dataGridView1.SelectedCells[0].RowIndex;
                    //Se cambio riga ritorno in output l'oggetto contenuto nella riga precedente
                    if (newCurrentlySelectedRow != _currentlySelectedRow)
                    {
                        Object selectedRow = GetObjectInTheCurrentlySelectedRow();
                        _currentlySelectedRow = newCurrentlySelectedRow;
                        return selectedRow;
                    }
                }
            }
            return null;
        }

        public void DaleteCurrentlySelectedRow()
        {
            if (_currentlySelectedRow >= 0)
            {
                dataGridView1.Rows.RemoveAt(_currentlySelectedRow);
                _currentlySelectedRow = -1;
            }
        }

        public ContextMenuStrip PopUpMenuForDeleting(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenuStrip menu = new ContextMenuStrip();
                menu.Items.Add("Cancella");

                int currentMouseOverRow = dataGridView1.HitTest(e.X, e.Y).RowIndex;

                if (currentMouseOverRow >= 0)
                {
                    _currentlySelectedRow = currentMouseOverRow;
                    //menu.MenuItems.Add(new MenuItem(string.Format("Do something to row {0}", currentMouseOverRow.ToString())));
                    menu.Show(dataGridView1, new Point(e.X, e.Y));
                    menu.AutoClose = true;
                    return menu;
                }
            }
            return null;
        }

        /*private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedCells.Count > 0)
            //if(dataGridView1.SelectedRows.Count > 0)
            {
                int selectedrowindex = dataGridView1.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dataGridView1.Rows[selectedrowindex];
                Debug.WriteLine($"{selectedRow.DataBoundItem}");
                string a = Convert.ToString(selectedRow.Cells["Name"].Value);
                //MessageBox.Show(a);
            }
        }*/
    }
}
