﻿using StudentsWithGUITable.Views;
using StudentsWithGUITable.Models;
using StudentsWithGUITable.Controllers;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentsWithGUITable
{
    static class Program
    {
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            
            Controller c = new Controller(new StudentDao(), new TableView());
            c.Start();
        }
    }
}
